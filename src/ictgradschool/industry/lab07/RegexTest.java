package ictgradschool.industry.lab07;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ljam763 on 27/03/2017.
 */
public class RegexTest {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        Pattern pattern = Pattern.compile("([^@]+@([^.]+\\.)+[^.]+)");
        Matcher m = pattern.matcher("tyan049@aucklanduni.ac.nz");
        System.out.println(Math.PI);
        if (m.find()) {
            System.out.println(m.group(0));
            System.out.println(m.group(1));
            System.out.println(m.group(2));
        }
    }
}
