package ictgradschool.industry.lab07.ex02;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    /**
     * Plays the actual guessing game.
     * <p>
     * You shouldn't need to edit this method for this exercise.
     */
    public void start() {

        int number = getRandomValue();
        int guess = 0;

        while (guess != number) {

            guess = getUserGuess();

            if (guess > number) {
                System.out.println("Too high!");
            } else if (guess < number) {
                System.out.println("Too low!");
            } else {
                System.out.println("Perfect!");
            }

        }

    }

    /**
     * Gets a random integer between 1 and 100.
     * <p>
     * You shouldn't need to edit this method for this exercise.
     */
    private int getRandomValue() {
        return (int) (Math.random() * 100) + 1;
    }

    /**
     * Gets the user's guess from the keyboard. Currently assumes that the user will always enter a valid guess.
     * <p>
     * TODO Implement some error handling, for the cases where the user enters a value that's too big, too small, or
     * TODO not an integer. Change this method so it's guaranteed to return an integer between 1 & 100, inclusive.
     */
    private int getUserGuess() {
        int number_input = 0;
        while (number_input == 0) {
            System.out.print("Enter your guess: ");
            try {
                number_input = Integer.parseInt(Keyboard.readInput());
            } catch (NumberFormatException e) {
                System.out.println("Please input an integer!");
                number_input = 0;
                continue;
            }
            try {
                if (number_input > 100 || number_input < 1) {
                    throw new Exception();
                }
            } catch (Exception e) {
                System.out.println("Please input an integer between 1 and 100.");
                number_input =0;
            }


        }
        return number_input;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
