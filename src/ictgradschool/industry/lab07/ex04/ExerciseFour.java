package ictgradschool.industry.lab07.ex04;

import ictgradschool.Keyboard;

import javax.swing.plaf.synth.SynthEditorPaneUI;

public class ExerciseFour {
    /**
     * Main program entry point. Do not edit, except for uncommenting the marked lines when required.
     */
    public static void main(String[] args) {
        ExerciseFour program = new ExerciseFour();

        // Exercise four, Question 1
        program.divideNumbers();

        // Exercise four, Question 2
        // TODO You may uncomment this line to help test your solution to question two.
        program.question2();

        // Exercise four, Question 3
        // TODO You may uncomment this line to help test your solution to question three.
        program.question3();
    }

    /**
     * The following tries to divide using two user input numbers, but is prone to error.
     * <p>
     * TODO Add some error handling to the code as follows:
     * TODO 1) If the user enters 0 for the second number, "Divide by 0!" should be printed instead of crashing the program.
     * TODO 2) If the user enters numbers which aren't integers, "Input error!" should be printed instead of crashing the program.
     * TODO Both these exceptional cases should be handled in the same try-catch block.
     */
    public void divideNumbers() {
        int num1 = 0;
        int num2 = 0;
        try {
            System.out.print("Enter the first number: ");
            String str1 = Keyboard.readInput();
            num1 = Integer.parseInt(str1);
            System.out.print("Enter the second number: ");
            String str2 = Keyboard.readInput();
            num2 = Integer.parseInt(str2);
            System.out.println(num1 + " / " + num2 + " = " + (num1 / num2));
        } catch (NumberFormatException e) {
            System.out.println("Input error!");
        } catch (ArithmeticException e) {
            System.out.println("Divide by 0!");
        }
        // Output the result
    }


    public void question2() {
        //TODO Write some Java code which throws a StringIndexOutOfBoundsException
        String testing = "";
        try {
            System.out.print("Please enter a word for checking.");
            testing = Keyboard.readInput();
            System.out.print("Please enter a number to test String index.");
            int number = Integer.parseInt(Keyboard.readInput());
            char a = testing.charAt(number);
            System.out.println(testing);
        } catch (StringIndexOutOfBoundsException e) {
            System.out.println("There is a string index out of bounds exception!!!");
        } catch (NumberFormatException e) {
            System.out.println("Please enter a testing number!");
        }
    }

    public void question3() {
        //TODO Write some Java code which throws a ArrayIndexOutOfBoundsException
        String[] array_of_words = new String[3];
        System.out.println("Please enter " + array_of_words.length + " words.");
        for (int i = 0; i < array_of_words.length; i++) {
            array_of_words[i] = Keyboard.readInput();
        }
        try {
            System.out.print("Check the words entered based on arrays number within 0 to " + array_of_words.length);
            int number = Integer.parseInt(Keyboard.readInput());
            System.out.println("The word is " + array_of_words[number]);
        } catch (NumberFormatException e) {
            System.out.println("Please enter a number/ integer.");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Out of bounds for the array.");
        }
    }
}