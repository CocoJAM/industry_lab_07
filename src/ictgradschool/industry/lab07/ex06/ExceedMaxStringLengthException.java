package ictgradschool.industry.lab07.ex06;

/**
 * Created by ljam763 on 27/03/2017.
 */
public class ExceedMaxStringLengthException extends parent_class_customs_exception{
    public String getMessage() {
        return "The string is more than 100 characters excluding the spaces.";
    }
}
