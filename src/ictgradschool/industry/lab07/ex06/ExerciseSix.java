package ictgradschool.industry.lab07.ex06;

import ictgradschool.Keyboard;

/**
 * TODO Write a program according to the Exercise Six guidelines in your lab handout.
 */
public class ExerciseSix {

    public void start() {
        // TODO Write the codes :)
        String input_from_user = userinput();
        try {
            String answer = first_letter(input_from_user);
            System.out.print("You entered: " + answer);
        } catch (parent_class_customs_exception e) {
            System.out.println("Error " + e.getMessage());
        }
    }

    // TODO Write some methods to help you.
    public String userinput() {
        System.out.println("Enter a string of at most 100 characters: ");
        return Keyboard.readInput();
    }

    public String first_letter(String sentence) throws parent_class_customs_exception {
        if (sentence.replaceAll(" ", "").length() > 100) {
            throw new ExceedMaxStringLengthException();
        }
        String[] list_of_words = sentence.split(" ");
        String first_letter = "";
        for (int i = 0; i < list_of_words.length; i++) {
            try {
                int number = Integer.parseInt("" + list_of_words[i].charAt(0));
            } catch (NumberFormatException e) {
                first_letter += list_of_words[i].toUpperCase().charAt(0) + " ";
                continue;
            }
            throw new InvalidWordException();
        }

        return first_letter.trim();
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        new ExerciseSix().start();
    }
}
